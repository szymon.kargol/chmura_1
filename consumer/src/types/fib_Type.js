const avro = require('avsc');

const avroSchema = {
  name: 'Fib',
  type: 'record',
  fields: [
    {
      name: 'k',
      type: 'long',
    },
    {
      name: 'result',
      type: 'long',
    },
  ],
};

const fib_Type = avro.parse(avroSchema);

module.exports = fib_Type;
