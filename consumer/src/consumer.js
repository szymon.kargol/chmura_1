const kafka = require('kafka-node');
const fib_Type = require('./types/fib_Type');
const dbo = require('./conn');

dbo.connectToServer(function (err) {
  if (err) {
    console.error(err);
    process.exit();
  }

  go();
})

function fib(index) {
  if (index < 2) return 1;
  return fib(index - 1) + fib(index - 2);
}

const go = async () => {
  const kafkaClientOptions = {sessionTimeout: 100, spinDelay: 100, retries: 2};
  const kafkaClient = new kafka.Client(process.env.KAFKA_ZOOKEEPER_CONNECT,
      'consumer-client', kafkaClientOptions);

  const topics = [
    {topic: 'fibonacci'},
  ];

  const options = {
    autoCommit: true,
    fetchMaxWaitMs: 1000,
    fetchMaxBytes: 1024 * 1024,
    encoding: 'buffer',
  };

  const kafkaConsumer = new kafka.HighLevelConsumer(kafkaClient, topics,
      options);

  kafkaConsumer.on('message', async function(message) {
    console.log('Message received:', message);
    const messageBuffer = new Buffer(message.value, 'binary');

    const decodedMessage = fib_Type.fromBuffer(messageBuffer.slice(0));
    console.log('Decoded Message:', typeof decodedMessage, decodedMessage);

    const {k} = decodedMessage;
    const result = fib(k);
    console.log('Kalkulator:' + result);

    const dbConnect = dbo.getDb();
    await dbConnect.collection('fibo').
        insertOne({k, result}, function(err, result) {
          if (err) {
            console.log('Error inserting matches!');
          } else {
            console.log(`Added a new match with id ${result.insertedId}`);
          }
        });

    console.log('Insert Response:', result);
  });

  kafkaClient.on('error',
      (error) => console.error('Kafka client error:', error));
  kafkaConsumer.on('error',
      (error) => console.error('Kafka consumer error:', error));
};
