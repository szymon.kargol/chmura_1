const express = require('express');
const router = express.Router();
const kafka = require('kafka-node');
const fib_Type = require('./../types/fib_Type');
const dbo = require('./../conn');

const kafkaClientOptions = {sessionTimeout: 100, spinDelay: 100, retries: 2};
const kafkaClient = new kafka.Client(process.env.KAFKA_ZOOKEEPER_CONNECT,
    'producer-client', kafkaClientOptions);
const kafkaProducer = new kafka.HighLevelProducer(kafkaClient);

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

kafkaClient.on('error', (error) => console.error('Kafka client error:', error));
kafkaProducer.on('error',
    (error) => console.error('Kafka producer error:', error));

router.get('/history', async (req, res) => {
  const dbConnect = dbo.getDb();
  const collection = dbConnect.collection('fibo');

  collection.
      find().
      limit(10).
      sort({_id: -1}).
      toArray(function(err, result) {
        if (err) {
          return res.status(400).send('Error fetching listings!');
        } else {
          return res.json(result);
        }
      });
});

router.get('/:k', function(req, res, next) {
  const k = Number(req.params.k);

  if (k > 30) {
    res.status(400);
    return res.send('k too big');
  }

  const messageBuffer = fib_Type.toBuffer({
    k,
    result: 0,
  });

  const payload = [
    {
      topic: 'fibonacci',
      messages: messageBuffer,
      attributes: 1,
    }];

  kafkaProducer.send(payload, async function(error, result) {
    console.info('Sent payload to SKafka:', payload);

    if (error) {
      console.error('Sending payload failed:', error);
      res.status(500).json(error);
    } else {
      console.log('Sending payload result:', result);
      await delay(500);

      const dbConnect = dbo.getDb();

      dbConnect.collection('fibo').
          find({k}).
          limit(1).
          toArray(function(err, result) {
            if (err) {
              return res.status(400).send('Error fetching listings!');
            } else {
              return res.json(result);
            }
          });
    }
  });
});

module.exports = router;
