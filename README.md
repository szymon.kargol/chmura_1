# Zadanie 1
#### by Szymon Kargol

## Opis

Wykonane według własnego pomysłu.

---

### Serwisy

1. haproxy - proxy które upublicznia aplikację frontendową na vuejs oraz api backendowe
2. vue - aplikacja frontendowa
3. express - aplikacja na node js, służy jako _api_
4. mongo - baza danych do przechowywania obliczeń
5. consumer - worker wykonuje obliczenia
6. kafka - przekazuje wiadomości pomięczy _api_ oraz consumerem (_workerem_)
7. zookeper - zarządza usługą _kafki_ (było to konieczne, można uznać to jako jedna wspólna usługa z kafką)

### Schemat 

![](vue/public/doc/Zadanie1_Schema.png)

---

## Uruchomienie

1. W katalogu głównym projektu `$ docker-compose up`
2. W przeglądarce `//localhost:3030`

### Podgląd aplikacji

![](vue/public/doc/Zadanie1_Browser.png)

---
